# -*-eselect-*-  vim: ft=sh
# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Initial version by Anna Figueiredo Gomes <anna@navirc.com>
# see: https://bugs.gentoo.org/882569

inherit config multilib

DESCRIPTION="Eselect module for management of multiple dotnet versions"
MAINTAINER="xgqt@gentoo.org"
VERSION="0.1.0"

remove_symlinks() {
	rm -f "${EROOT}"/usr/bin/dotnet
}

_dup() {
	dirname "${1}"/.
}

set_symlinks() {
	local ver="${1#dotnet}"
	local bin_prefix="${EROOT}"/usr/bin

	ln -s "dotnet${ver}" "$(_dup "${bin_prefix}"/dotnet)"
}

find_targets() {
	local prefix="${EROOT}"/usr/bin/
	local dirs

	local f
	for f in ${prefix}dotnet-* ; do
		[[ -f "${f}" ]] && dirs="${dirs} ${f##$prefix}"
	done

	echo "${dirs}"
}

resolve_target() {
	local -a targets=( $(find_targets) )

	if is_number "${1}" ; then
		[[ "${1}" -le ${#targets[@]} && "${1}" -gt 0 ]] &&
			echo "${targets[ $(( ${1} - 1 )) ]}"
	elif has "${1}" ${targets[@]} ; then
		echo "${1}"
	fi
}

get_active_version() {
	readlink "${EROOT}"/usr/bin/dotnet
}

describe_set() {
	echo "Sets the current version of dotnet"
}

describe_set_parameters() {
	echo "[--if-unset] <target>"
}

describe_set_options() {
	echo '--if-unset:  Do not replace currently selected implementation'
	echo 'target:      Target name or number (from "list" action)'
}

do_set() {
	if [[ "${1}" == "--if-unset" ]] ; then
		if [[ "$(get_active_version)" ]] ; then
			return
		fi
		shift
	fi

	local target="$(resolve_target "${1}")"
	if [[ ! "${target}" ]] ; then
		die -q "You need to specify a version"
	fi

	remove_symlinks

	set_symlinks "${target}"
}

describe_list() {
	echo "Lists available dotnet versions"
}

do_list() {
	local -a targets=( $(find_targets) )
	local active="$(get_active_version)"

	for (( i = 0 ; i < ${#targets[@]} ; i++ )) ; do
		[[ "${active}" == ${targets[i]} ]] &&
			targets[i]=$(highlight_marker "${targets[i]}")
	done

	write_numbered_list -m '(none found)' "${targets[@]}"
}

describe_show() {
	echo "Show the active dotnet version"
}

do_show() {
	get_active_version
}

describe_update() {
	echo "Automatically update the dotnet symlink"
}

describe_update_options() {
	echo "ifunset : Do not override currently set version"
}

do_update() {
	case "${1}" in
		"" | ifunset | --if-unset )
			:
			;;
		* )
			die -q "Usage error"
			;;
	esac

	[[ ${#} -gt 1 ]] && die -q "Too many parameters"

	if [[ -L "${EROOT}"/usr/bin/dotnet ]] ; then
		if [[ ${1} == *if*unset && -e "${EROOT}"/usr/bin/dotnet ]] ; then
			return
		fi
		remove_symlinks || die -q "Couldn't remove existing symlink"
	elif [[ -e "${EROOT}"/usr/bin/dotnet ]] ; then
		die -q "${EROOT}/usr/bin/dotnet exists but is not a symlink"
	fi

	local -a targets=( $(find_targets) )
	if [[ ${#targets[@]} -gt 0 ]] ; then
		set_symlinks "${targets[${#targets[@]}-1]}" ||
			die -q "Couldn't set a new symlink"
	fi
}

describe_unset() {
	echo "Remove the dotnet symlink"
}

do_unset() {
	if [[ -L "${EROOT}"/usr/bin/dotnet ]] ; then
		remove_symlinks || die -q "Couldn't remove existing symlink"
	elif [[ -e "${EROOT}"/usr/bin/dotnet ]] ; then
		die -q "${EROOT}/usr/bin/dotnet exists but is not a symlink"
	fi
}
